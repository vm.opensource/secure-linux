# Secure-Linux

A script which adds extra hardening and security changes to Linux.

# Usage

Run the file:
./src/run.py

# Compatible distributions

* Debian 8 + / Debian-based (stable and rolling)
* Arch Linux / Manjaro / Arch Linux-based
* Fedora 28 +
* RHEL / CentOS / Rocky linux / Alma linux / Oracle linux / Amazon Linux
* Ubuntu 18.04 + / Ubuntu-based
* Gentoo / Gentoo-based
* Void Linux / Void-linux based
* Slackware
* SUSE / OpenSUSE / Gecko Linux
* Solus OS
* Other Linux 4.19 +
