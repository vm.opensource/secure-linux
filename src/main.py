# (C) Copyright 2022 Venkatesh Mishra
# (C) Copyright 2022 Emph Corp
# file: main.py
# project: Secure-linux
# python version: 3.10
# date: 02/03/2022

import os
import sys
import time

def kernel():
    print("Hardening kernel...")
    time.sleep(1)
    if __name__ == "__main__":
        os.system("sh kernel-secure.sh")
    else:
        pass

def net():
    print("Securing ipv4 protocols...")
    time.sleep(1)
    if __name__ == "__main__":
        os.system("sh net-secure.sh")
    else:
        pass

def fs():
    print("Securing filesystem...")
    time.sleep(1)
    if __name__ == "__main__":
        os.system("sh fs-secure.sh")
    else:
        pass

def vm():
    print("Adding extra hardening features...")
    time.sleep(1)
    if __name__ == "__main__":
        os.system("sh vm-secure.sh")
    else:
        pass

if __name__ == "__main__":
	for i in range(1):
		kernel()
		net()
		fs()
		vm()

print("Done!")
sys.exit()
