sysctl kernel.printk=3 3 3 3
sysctl kernel.sysrq=4
sysctl kernel.kptr_restrict=2
sysctl kernel.unprivileged_bpf_disabled=1
sysctl kernel.kexec_load_disabled=1
sysctl kernel.unprivileged_userns_clone=0
sysctl kernel.perf_event_paranoid=3
sysctl kernel.yama.ptrace_scope=2
